package uk.co.gluedit.funcjava;

public abstract class Stream<T> {

    private static Stream EMPTY = new Empty();

    public abstract T head();

    public abstract Stream<T> tail();

    public abstract Boolean isEmpty();

    private static class Empty<T> extends Stream<T> {

        @Override
        public Stream<T> tail() {
            throw new IllegalStateException("tail called on empty");
        }

        @Override
        public T head() {
            throw new IllegalStateException("head called on empty");
        }

        @Override
        public Boolean isEmpty() {
            return true;
        }

    }

    private static class Cons<T> extends Stream<T> {

        private final Supplier<T> head;
        private T h;
        private final Supplier<Stream<T>> tail;
        private Stream<T> t;

        private Cons(Supplier<T> h, Supplier<Stream<T>> t) {
            head = h;
            tail = t;
        }

        @Override
        public T head() {
            if (h == null)
                h = head.get();
            return h;
        }

        @Override
        public Stream<T> tail() {
            if (t == null)
                t = tail.get();
            return t;
        }

        @Override
        public Boolean isEmpty() {
            return false;
        }

    }

    static <T> Stream<T> cons(Supplier<T> hd, Supplier<Stream<T>> tl) {
        return new Cons<>(hd, tl);
    }

    @SuppressWarnings("unchecked")
    public static <T> Stream<T> empty() {
        return EMPTY;
    }

    public static Stream<Integer> from(int i) {
        return cons(() -> i, () -> from(i + 1));
    }

}

