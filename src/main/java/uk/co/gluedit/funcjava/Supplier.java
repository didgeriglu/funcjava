package uk.co.gluedit.funcjava;

public interface Supplier<T> {
    T get();
}
