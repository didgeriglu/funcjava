package uk.co.gluedit;

import uk.co.gluedit.funcjava.Stream;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Stream<Integer> stream = Stream.from(1);
        System.out.println(stream.head());
        System.out.println(stream.tail());
        System.out.println(stream.tail().head());
        System.out.println(stream.tail().tail().head());
    }
}
